
package day6.main;

import day6.classes.DemoThread1;


public class DemoThread1Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DemoThread1 demoThread1 = new DemoThread1();
		demoThread1.run();
		DemoThread1 demoThread2 = new DemoThread1();
		demoThread2.run();
		DemoThread1 demoThread3 = new DemoThread1();
		demoThread3.run();
	}

}
