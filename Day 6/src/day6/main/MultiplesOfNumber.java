
package day6.main;

import day6.classes.Number;
import day6.impl.NumberImpl;


public class MultiplesOfNumber {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Number number = new NumberImpl();
		number.run(2);
		Number number1 = new NumberImpl();
		number1.run(5);
		Number number2 = new NumberImpl();
		number2.run(8);
	}

	

}
