
package day6.main;

import java.time.LocalTime;


public class WithoutTryAndCatch {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public void run() throws InterruptedException {
		
		Thread.sleep(10000);
		LocalTime time2 = LocalTime.now();
		
		System.out.println("After the Sleep Current Time : " + time2);
	}

	public static void main(String[] args) {
		RenameThread renameThread = new RenameThread();
		System.out.println("Current Thread Name : " + renameThread.getName());
		renameThread.setName("My Thread");
		LocalTime time = LocalTime.now();
		System.out.println("Before the sleep Current Time : " + time);
		renameThread.start();
		System.out.println("Newly changed thread Name : " + renameThread.getName());
	}
}
