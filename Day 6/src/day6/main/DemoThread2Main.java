
package day6.main;

import day6.classes.DemoThread2;


public class DemoThread2Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DemoThread2 demoThread1 = new DemoThread2();
		demoThread1.start();
		DemoThread2 demoThread2 = new DemoThread2();
		demoThread2.start();
		DemoThread2 demoThread3 = new DemoThread2();
		demoThread3.start();
		
	}
}
