package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.Account;

@Controller
public class Test {

	@GetMapping(value = "user")
	public ModelAndView initUser() {
		Account account = new Account();
		ModelAndView modelAndView = new ModelAndView("createAccount");
		modelAndView.addObject("account", account);
		return modelAndView;
	}

	@PostMapping(value = "addAccount")
	public ModelAndView createAccount(@ModelAttribute("account") Account account) {
		ModelAndView modelAndView = new ModelAndView("loginSuccess");
		System.out.println("UserName : " + account.getUserName() + "\n" + "Age : " + account.getAge() + "\n"
				+ "Place : " + account.getPlace());
		return modelAndView;
	}

	@GetMapping(value = "loginNew")
	public ModelAndView goToLogin() {
		ModelAndView modelAndView = new ModelAndView("login");

		return modelAndView;
	}

	@GetMapping(value = "registration")
	public ModelAndView gotoRegistration() {
		ModelAndView modelAndView = new ModelAndView("registrationPage");

		return modelAndView;
	}

	@GetMapping(value = "login")
	public ModelAndView gotoLogin() {
		ModelAndView modelAndView = new ModelAndView("loginPage");

		return modelAndView;
	}

	@GetMapping(value = "addPet")
	public ModelAndView gotoaddPet() {
		ModelAndView modelAndView = new ModelAndView("addPetPage");

		return modelAndView;
	}
	@GetMapping(value = "mypets")
	public ModelAndView gotomyPets() {
		ModelAndView modelAndView = new ModelAndView("myPetsPage");

		return modelAndView;
	}
}