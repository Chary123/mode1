<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Pets Page</title>
</head>
<body>
	<form action="addPet" method="get">
		<table>
			<tr>
				<td><label for="name">Name:</label></td>
				<td>
					<div>
						<input type="text" class="form-control" id="name">

					</div>
				</td>
			</tr>
			<tr>
				<td><label for="age">Age:</label></td>
				<td>
					<div class="form-group">
						<input type="text" class="form-control" id="age">
					</div>
				</td>
			</tr>

			<tr>
				<td><label for="place">Place:</label></td>

				<td>
					<div class="form-group">
						<input type="text" class="form-control" id="Place">
					</div>
				</td>
			</tr>
			<tr>
			<td><button type="submit" class="btn btn-default">Add Pet</button></td>
			<td><button type="reset" class="btn btn-default">Cancel</button></td>
			</tr>
		</table>
	</form>

</body>
</html>