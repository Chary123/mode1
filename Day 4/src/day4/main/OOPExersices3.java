/**
 * 
 */
package day4.main;


import day4.classes.ClassA;
import day4.classes.ClassB;
import day4.classes.ClassC;

/**
 * @author Sandeep
 *
 */
public class OOPExersices3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ClassA objA = new ClassA(); 
        ClassB objB1 = new ClassB(); 
        ClassA objB2 = new ClassB(); 
        ClassC objC1 = new ClassC(); 
        ClassB objC2 = new ClassC(); 
        ClassA objC3 = new ClassC(); 
        objA.display(); 
        objB1.display(); 
        objB2.display(); 
        objC1.display(); 
        objC2.display(); 
        objC3.display();    


	}

}
