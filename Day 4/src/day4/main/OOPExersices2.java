/**
 * 
 */
package day4.main;

import day4.classes.FirstClass;
import day4.classes.SecondClass;

/**
 * @author Sandeep
 *
 */
public class OOPExersices2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	      FirstClass firstClass = new FirstClass(); 
	      SecondClass secondClass = new SecondClass(); 
	      System.out.println("----------------in main(): "); 
	      System.out.println("objA.a = "+firstClass.getFirstClass()); 
	      System.out.println("objB.b = "+secondClass.getSecondClass()); 
	      System.out.println("-----------------After the Updation ");
	      firstClass.setFirstClass (222); 
	      secondClass.setSecondClass (333.33); 
	      System.out.println("objA.a = "+firstClass.getFirstClass()); 
	      System.out.println("objB.b = "+secondClass.getSecondClass()); 
	    } 



}
