/**
 * 
 */
package day4.main;

import java.util.Scanner;

import day4.Impl.Mycalculator;
import day4.interfaces.AdvancedArthimetic;

/**
 * @author Sandeep
 *
 */
public class AdvancedArthimeticMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AdvancedArthimetic mycalculator = new Mycalculator();
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Number : ");
		int number = scanner.nextInt();
		System.out.println("The Divisor Sum of the Number "+number+" is : "+ mycalculator.divisorSum(number));
		
		scanner.close();
		
	}

}
