/**
 * 
 */
package day4.interfaces;

/**
 * @author Sandeep
 *
 */
public interface AdvancedArthimetic {
	int divisorSum (int number);
}
