package com.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.Pet;
import com.model.User;
import com.service.UserValidate;

@Controller
public class mycontroller {

	@Autowired
	private UserValidate validates;

	@GetMapping("/")
	public ModelAndView init(User user) {
		ModelAndView modelAndView = new ModelAndView("NewFile");
		modelAndView.addObject("User", user);

		return modelAndView;
	}

	@GetMapping("login")
	public ModelAndView login(User user) {
		ModelAndView modelAndView = new ModelAndView("NewFile1");
		modelAndView.addObject("User", user);

		return modelAndView;
	}

	@GetMapping("home")
	public ModelAndView home(Pet pet) {
		ModelAndView modelAndView = new ModelAndView("home");
		modelAndView.addObject("Pet", pet);

		return modelAndView;
	}

	@GetMapping("buy")
	public ModelAndView buypet(Pet pet, HttpServletRequest request, User user) {
		ModelAndView modelAndView = new ModelAndView();
		String id = request.getParameter("petid");
		int petid = Integer.parseInt(id);
		System.out.println(user.getUSERID());
		int userid = user.getUSERID();
		validates.BuyPet(petid, pet, user);

		modelAndView.setViewName("success");
		return modelAndView;
	}

	
	@GetMapping(value = "addpet")
	public ModelAndView home(@Valid @ModelAttribute("Pet") Pet pet, BindingResult bindingResult) throws IOException {
		ModelAndView modelAndView = new ModelAndView();

		System.out.println(pet.getPETNAME());
		if (bindingResult.hasErrors()) {

			modelAndView.setViewName("home");

		} else {
			Pet addpet = validates.AddPet(pet);
			modelAndView.addObject("key", pet);
			modelAndView.setViewName("home1");

		}

		return modelAndView;
	}

	

}
