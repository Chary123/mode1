package com.service;




import java.util.List;

import com.model.Pet;
import com.model.User;


public interface UserValidate {
	
public abstract User SaveUser(User user);

public abstract Boolean AuthenticateUser(User user);

public abstract List<Pet> ViewPet(Pet pet);

public abstract Pet AddPet(Pet pet);
public abstract Pet BuyPet(int petid,Pet pet,User user);

}
