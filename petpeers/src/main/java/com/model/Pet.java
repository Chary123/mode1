package com.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;



@Entity

@Table(name="pet")
public class Pet implements Serializable {
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private int PETID;
	@NotEmpty
	private String PETNAME;
	@NotEmpty
	private String PETAGE;
	@NotEmpty
	private String PETPLACE;
	@ManyToOne(cascade = {CascadeType.ALL})
    private User user;

	
	public int getPETID() {
		return PETID;
	}
	
	public Pet(int pETID, @NotEmpty String pETNAME, @NotEmpty String pETAGE, @NotEmpty String pETPLACE, User user) {
	super();
	PETID = pETID;
	PETNAME = pETNAME;
	PETAGE = pETAGE;
	PETPLACE = pETPLACE;
	this.user = user;
}

	public Pet() {
		super();
		
	}
	public void setPETID(int pETID) {
		PETID = pETID;
	}
	public String getPETNAME() {
		return PETNAME;
	}
	public void setPETNAME(String pETNAME) {
		PETNAME = pETNAME;
	}
	public String getPETAGE() {
		return PETAGE;
	}
	public void setPETAGE(String pETAGE) {
		PETAGE = pETAGE;
	}
	public String getPETPLACE() {
		return PETPLACE;
	}
	public void setPETPLACE(String pETPLACE) {
		PETPLACE = pETPLACE;
	}
	public User getUser() {
		return user;
	}
	public void setPet(User user) {
		this.user = user;
	}
	
}
