package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;

import com.model.Pet;
import com.model.User;


@Repository
public class UserDaoServicesImpl implements UserDao {
	  @Autowired
	   	private SessionFactory sessionFactory;
	   	
	   	
	   	public SessionFactory getSessionFactory() {
	   			return sessionFactory;
	   		}

	   		public void setSessionFactory(SessionFactory sessionFactory) {
	   			this.sessionFactory = sessionFactory;
	   		}
	       
	@Override
	public User SaveUser(User user) {
	
		
		Session session = this.sessionFactory.getCurrentSession();
		


		Query query = session.createQuery("FROM User e WHERE e.USERNAME= ?0");
		query.setParameter(0, user.getUSERNAME());
		List<User> results = query.getResultList();
	
      if(results.isEmpty()) {
	 
	       session.save(user);
	        return user;
          }
 else {
	
	 user.setUSERNAME(null);
 }

	return user;
		}


	@Override
	public Boolean userDaoauthenticateUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		


		Query query = session.createQuery("FROM User e WHERE e.USERNAME= ?0 and e.USERPASSWORD= ?1");
		query.setParameter(0, user.getUSERNAME());
		query.setParameter(1, user.getUSERPASSWORD());
		List<User> results=query.getResultList();
		
		if(results.isEmpty())
		{
			
			return false;
		}
			
			
		else
		{Query query1 = session.createQuery("FROM User e WHERE e.USERNAME= ?0 ");
		query1.setParameter(0, user.getUSERNAME());
		List<User> results1=query1.getResultList();
		for (User user2 : results1) {
			user.setUSERID(user2.getUSERID());
		}
			return true;
			
		}
			

		
	}

	@Override
	public Pet AddPet(Pet pet) {
		Session session = this.sessionFactory.getCurrentSession();
		
		session.save(pet);
		Query query = session.createQuery("FROM Pet");
		List<Pet> results=query.getResultList();
		
		for (Pet pet2 : results) {
			System.out.println(pet2.getPETNAME());
		}
		return null;
	}

	@Override
	public List<Pet> ViewPet(Pet pet) {
		Session session = this.sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("FROM Pet ");
		List<Pet> results=query.getResultList();
		for (Pet pet2 : results) {
			System.out.println(pet2.getPETNAME());
		}
		
		return results;
	}

	
}
		

