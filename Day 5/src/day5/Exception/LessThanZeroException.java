/**
 * 
 */
package day5.Exception;


public class LessThanZeroException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LessThanZeroException(String s) {
		super(s);
	}

}
