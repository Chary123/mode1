/**
 * 
 */
package day5.main;

import java.util.Scanner;

import day5.Exception.LessThanZeroException;
import day5.classes.MyCalculator;


public class FindPowerOf {

	/**
	 * @param args
	 * @throws LessThanZeroException 
	 */
	public static void main(String[] args) throws LessThanZeroException {
		Scanner scanner = new Scanner(System.in);
		
			System.out.println("Enter the Numbers");
			int num1 = scanner.nextInt();
			int num2 = scanner.nextInt();
			MyCalculator myCalculator = new MyCalculator();
			try {
				System.out.println("Power of Two Numbers : "+myCalculator.Power(num1, num2));
			}catch(LessThanZeroException ltxe) {
				System.err.println(ltxe.getMessage());
			}
			
		
		scanner.close();
	}

}
