/**
 * 
 */
package day5.main;

import java.util.Scanner;

import day5.Exception.CustomException;


public class IPLPlayerAge {

	/**
	 * @param args
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) throws CustomException{
		Scanner scanner = new Scanner(System.in);
		
		try {
		System.out.println("Enter The Player Name :" );
		String playerName = scanner.nextLine();
		System.out.println("Enter The Player Age :" );
		int playerAge = scanner.nextInt();
		if(playerAge< 19) {
			throw new CustomException("InvalidAgeRange");
		}
		
		System.out.println("Enter The Player Name : " + playerName);
		System.out.println("Enter The Player Age : " +playerAge);
		}
		catch(CustomException ce) {
			System.err.println(ce.getMessage());
		}
		scanner.close();
		
		

	}

}
