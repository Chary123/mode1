<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home Page</title>
</head>
<body>
	<form action="mypets" method="get">

		<table class="table table-bordered">

			<!-- Navigation -->
			<nav class="w3-bar w3-black"> Pet Shop &nbsp <a
				href="/petpeersproject/homePage" class="w3-button w3-bar-item">Home</a>
			&nbsp; <a href="/petpeersproject/myPets"
				class="w3-button w3-bar-item">My Pets</a> &nbsp; <a
				href="/petpeersproject/addPet" class="w3-button w3-bar-item">Add
				Pets</a>
			<div align="right"> 
				<a href="/petpeersproject/logout" class="w3-button w3-bar-item">Logout</a>
				&nbsp;
				</div>
			</nav>
			
			<tr>
				<th>Pet ID</th>
				<th>Pet Name</th>
				<th>Pet Age</th>
				<th>Pet Place</th>
				<th>Buy</th>
			</tr>
			<tr>
				<td>1</td>
				<td>Tommy</td>
				<td>2</td>
				<td>HYD</td>
				<td>Buy</td>
			</tr>
			<tr>
				<td>2</td>
				<td>Chommy</td>
				<td>1</td>
				<td>BNGLR</td>
				<td>Sold Out</td>
			</tr>
		</table>
	</form>
</body>
</html>